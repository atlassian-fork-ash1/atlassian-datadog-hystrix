package com.atlassian.datadog.hystrix;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

class TestCommand extends HystrixCommand<TestCommand.Result> {
    enum Result {CLEAN_PASS, FALLBACK_PASS, FAILURE}

    private static final Logger log = LoggerFactory.getLogger(TestCommand.class);

    private final Result result;

    TestCommand(Result result) {
        super(HystrixCommandGroupKey.Factory.asKey("RandomGroup"),
            HystrixThreadPoolKey.Factory.asKey("RandomPool"));
        this.result = requireNonNull(result);
    }

    @Override
    protected Result run() {
        log.info("run() called with action {}", result);
        if (result != Result.CLEAN_PASS) {
            throw new RuntimeException("told to: " + result);
        }
        return Result.CLEAN_PASS;
    }

    @Override
    protected Result getFallback() {
        if (result != Result.FALLBACK_PASS) {
            throw new RuntimeException("told to: " + result);
        }
        return Result.FALLBACK_PASS;
    }
}
