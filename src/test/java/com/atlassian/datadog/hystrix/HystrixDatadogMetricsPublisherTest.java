package com.atlassian.datadog.hystrix;

import com.netflix.hystrix.Hystrix;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.netflix.hystrix.strategy.HystrixPlugins;
import com.timgroup.statsd.StatsDClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.longThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class HystrixDatadogMetricsPublisherTest {
    @SuppressWarnings("NullableProblems")
    @Mock
    private StatsDClient client;

    @Before
    public void setUp() throws Exception {
        Hystrix.reset();
        HystrixPlugins.getInstance().registerMetricsPublisher(new HystrixDatadogMetricsPublisher(client));
    }

    @Test
    public void testCleanPass() throws Exception {
        final TestCommand tc = new TestCommand(TestCommand.Result.CLEAN_PASS);
        final TestCommand.Result result = tc.execute();

        assertThat(result, is(TestCommand.Result.CLEAN_PASS));

        verify(client).recordExecutionTime(
            eq("hystrix.command.latency.total.time"),
            longThat(is(greaterThanOrEqualTo(0L))),
            eq("commandKey:TestCommand"),
            eq("commandGroupKey:RandomGroup"),
            eq("threadPoolKey:RandomPool"),
            eq("commandCompletion:true"),
            eq("executedInThread:true"),
            eq("executionStart:false"),
            eq("responseThreadPoolRejected:false"),
            eq("terminalEvent:SUCCESS"));

        verifyCommonStuff();
        verifyNoMoreInteractions(client);
    }

    @Test
    public void testFallbackPass() throws Exception {
        final TestCommand tc = new TestCommand(TestCommand.Result.FALLBACK_PASS);
        final TestCommand.Result result = tc.execute();

        assertThat(result, is(TestCommand.Result.FALLBACK_PASS));

        verify(client).recordExecutionTime(
            eq("hystrix.command.latency.total.time"),
            longThat(is(greaterThanOrEqualTo(0L))),
            eq("commandKey:TestCommand"),
            eq("commandGroupKey:RandomGroup"),
            eq("threadPoolKey:RandomPool"),
            eq("commandCompletion:true"),
            eq("executedInThread:true"),
            eq("executionStart:false"),
            eq("responseThreadPoolRejected:false"),
            eq("terminalEvent:FALLBACK_SUCCESS"));

        verifyCommonStuff();
        verifyNoMoreInteractions(client);
    }

    @Test
    public void testFailure() {
        try {
            final TestCommand tc = new TestCommand(TestCommand.Result.FAILURE);
            final TestCommand.Result result = tc.execute();
            fail("Exception is expected");
        } catch (HystrixBadRequestException | HystrixRuntimeException e) {
            // expected
        }

        verify(client).recordExecutionTime(
            eq("hystrix.command.latency.total.time"),
            longThat(is(greaterThanOrEqualTo(0L))),
            eq("commandKey:TestCommand"),
            eq("commandGroupKey:RandomGroup"),
            eq("threadPoolKey:RandomPool"),
            eq("commandCompletion:true"),
            eq("executedInThread:true"),
            eq("executionStart:false"),
            eq("responseThreadPoolRejected:false"),
            eq("terminalEvent:FALLBACK_FAILURE"));

        verifyCommonStuff();
        verifyNoMoreInteractions(client);
    }

    private void verifyCommonStuff() {
        verify(client).recordGaugeValue(
            eq("hystrix.command.concurrent.execution.count"),
            longThat(is(greaterThanOrEqualTo(0L))),
            eq("commandKey:TestCommand"),
            eq("commandGroupKey:RandomGroup"));

        verify(client).recordGaugeValue(
            eq("hystrix.command.circuitBreaker.open"),
            eq(0L),
            eq("commandKey:TestCommand"),
            eq("commandGroupKey:RandomGroup"));

        verify(client).recordGaugeValue(
            eq("hystrix.threadPool.active.count"),
            eq(1L),
            eq("threadPoolKey:RandomPool"));

        verify(client).recordGaugeValue(
            eq("hystrix.threadPool.pool.size"),
            eq(1L),
            eq("threadPoolKey:RandomPool"));

        verify(client).recordGaugeValue(
            eq("hystrix.threadPool.queue.size"),
            eq(0L),
            eq("threadPoolKey:RandomPool"));
    }
}
