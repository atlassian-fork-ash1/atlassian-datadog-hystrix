package com.atlassian.datadog.hystrix;

import com.netflix.hystrix.HystrixCollapserKey;
import com.netflix.hystrix.HystrixCollapserMetrics;
import com.netflix.hystrix.HystrixCollapserProperties;
import com.netflix.hystrix.strategy.metrics.HystrixMetricsPublisherCollapser;
import com.timgroup.statsd.StatsDClient;

import static java.util.Objects.requireNonNull;

class HystrixDatadogMetricsPublisherCollapser implements HystrixMetricsPublisherCollapser {
    private final HystrixCollapserKey collapserKey;
    private final HystrixCollapserMetrics metrics;
    private final HystrixCollapserProperties properties;
    private final StatsDClient client;

    HystrixDatadogMetricsPublisherCollapser(HystrixCollapserKey collapserKey, HystrixCollapserMetrics metrics, HystrixCollapserProperties properties, StatsDClient client) {
        this.collapserKey = requireNonNull(collapserKey);
        this.metrics = requireNonNull(metrics);
        this.properties = requireNonNull(properties);
        this.client = requireNonNull(client);
    }

    @Override
    public void initialize() {
        // TODO - need to implement
    }
}
