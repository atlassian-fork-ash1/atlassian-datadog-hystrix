package com.atlassian.datadog.hystrix;

import com.netflix.hystrix.ExecutionResult;
import com.netflix.hystrix.HystrixCircuitBreaker;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandMetrics;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixEventType;
import com.netflix.hystrix.metric.HystrixCommandCompletion;
import com.netflix.hystrix.metric.HystrixCommandCompletionStream;
import com.netflix.hystrix.strategy.metrics.HystrixMetricsPublisherCommand;
import com.timgroup.statsd.StatsDClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

class HystrixDatadogMetricsPublisherCommand implements HystrixMetricsPublisherCommand {
    private static final Logger log = LoggerFactory.getLogger(HystrixDatadogMetricsPublisherCommand.class);

    private final HystrixCommandKey commandKey;
    private final HystrixCommandGroupKey commandGroupKey;
    private final HystrixCommandMetrics metrics;
    private final HystrixCircuitBreaker circuitBreaker;
    private final StatsDClient client;

    HystrixDatadogMetricsPublisherCommand(HystrixCommandKey commandKey,
                                          HystrixCommandGroupKey commandGroupKey,
                                          HystrixCommandMetrics metrics,
                                          HystrixCircuitBreaker circuitBreaker,
                                          StatsDClient client) {
        this.commandKey = requireNonNull(commandKey);
        this.commandGroupKey = requireNonNull(commandGroupKey);
        this.metrics = requireNonNull(metrics);
        this.circuitBreaker = requireNonNull(circuitBreaker);
        this.client = requireNonNull(client);
    }

    @Override
    public void initialize() {
        log.debug("initialize() called for {}/{}", commandGroupKey.name(), commandKey.name());
        HystrixCommandCompletionStream.getInstance(commandKey)
            .observe()
            .onBackpressureDrop()
            .subscribe(this::handle);
    }

    private void handle(HystrixCommandCompletion completion) {
        // WARNING: Atlassian's gostatsd does not support Histograms yet. So have to make do with execution time.
        //        client.recordHistogramValue(
        client.recordExecutionTime(
            "hystrix.command.latency.total.time",
            completion.getTotalLatency(),
            "commandKey:" + completion.getCommandKey().name(),
            "commandGroupKey:" + commandGroupKey.name(),
            "threadPoolKey:" + completion.getThreadPoolKey().name(),
            "commandCompletion:" + completion.isCommandCompletion(),
            "executedInThread:" + completion.isExecutedInThread(),
            "executionStart:" + completion.isExecutionStart(),
            "responseThreadPoolRejected:" + completion.isResponseThreadPoolRejected(),
            "terminalEvent:" + terminalEvent(completion.getEventCounts()));

        client.recordGaugeValue(
            "hystrix.command.concurrent.execution.count",
            metrics.getCurrentConcurrentExecutionCount(),
            "commandKey:" + commandKey.name(),
            "commandGroupKey:" + commandGroupKey.name());

        client.recordGaugeValue(
            "hystrix.command.circuitBreaker.open",
            circuitBreaker.isOpen() ? 1L : 0L,
            "commandKey:" + commandKey.name(),
            "commandGroupKey:" + commandGroupKey.name());
    }

    private String terminalEvent(ExecutionResult.EventCounts eventCounts) {
        for (HystrixEventType candidate : HystrixEventType.TERMINAL_EVENT_TYPES) {
            if (eventCounts.contains(candidate)) {
                return candidate.name();
            }
        }
        return "UNKNOWN";
    }
}
